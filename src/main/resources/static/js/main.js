var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
        $("#msg-form").show();
    }
    else {
        $("#conversation").hide();
        $("#msg-form").hide();
    }
    $("#chat").html("");
}

function connect() {
    var socket = new SockJS('/gs-guide-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        stompClient.subscribe('/queue', function(message) {
            message = JSON.parse(message.body);
            showChatMessage(message.author, message.content);
        });
    });
}

function send() {
    var msg = JSON.stringify({
        author: $("#name").val(),
        content: $("#msg").val()
    });
    if (stompClient !== null) {
        stompClient.send('/app/chat', {}, msg);
    }
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function showChatMessage(author, message) {
    $("#chat").append("<tr><td>" + author + "</td><td>" + message + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function () { send(); });
});